/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.sg.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.model.*;
import org.compiere.util.KeyNamePair;

/** Generated Interface for XX_LiquidationLine
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS
 */
public interface I_XX_LiquidationLine 
{

    /** TableName=XX_LiquidationLine */
    public static final String Table_Name = "XX_LiquidationLine";

    /** AD_Table_ID=1000028 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name Amt */
    public static final String COLUMNNAME_Amt = "Amt";

	/** Set Amount.
	  * Amount
	  */
	public void setAmt (BigDecimal Amt);

	/** Get Amount.
	  * Amount
	  */
	public BigDecimal getAmt();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Humedad */
    public static final String COLUMNNAME_Humedad = "Humedad";

	/** Set Humedad	  */
	public void setHumedad (BigDecimal Humedad);

	/** Get Humedad	  */
	public BigDecimal getHumedad();

    /** Column name HumedadN */
    public static final String COLUMNNAME_HumedadN = "HumedadN";

	/** Set Humedad Nueva	  */
	public void setHumedadN (BigDecimal HumedadN);

	/** Get Humedad Nueva	  */
	public BigDecimal getHumedadN();

    /** Column name HumedadW */
    public static final String COLUMNNAME_HumedadW = "HumedadW";

	/** Set Peso de Humedad	  */
	public void setHumedadW (BigDecimal HumedadW);

	/** Get Peso de Humedad	  */
	public BigDecimal getHumedadW();

    /** Column name ImpurezaN */
    public static final String COLUMNNAME_ImpurezaN = "ImpurezaN";

	/** Set Impureza Nueva	  */
	public void setImpurezaN (BigDecimal ImpurezaN);

	/** Get Impureza Nueva	  */
	public BigDecimal getImpurezaN();

    /** Column name Impurezas */
    public static final String COLUMNNAME_Impurezas = "Impurezas";

	/** Set Impurezas	  */
	public void setImpurezas (BigDecimal Impurezas);

	/** Get Impurezas	  */
	public BigDecimal getImpurezas();

    /** Column name ImpurezaW */
    public static final String COLUMNNAME_ImpurezaW = "ImpurezaW";

	/** Set Peso de Impureza	  */
	public void setImpurezaW (BigDecimal ImpurezaW);

	/** Get Peso de Impureza	  */
	public BigDecimal getImpurezaW();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name LiqAmt */
    public static final String COLUMNNAME_LiqAmt = "LiqAmt";

	/** Set Liquidation Amount	  */
	public void setLiqAmt (BigDecimal LiqAmt);

	/** Get Liquidation Amount	  */
	public BigDecimal getLiqAmt();

    /** Column name Price */
    public static final String COLUMNNAME_Price = "Price";

	/** Set Price.
	  * Price
	  */
	public void setPrice (BigDecimal Price);

	/** Get Price.
	  * Price
	  */
	public BigDecimal getPrice();

    /** Column name PriceN */
    public static final String COLUMNNAME_PriceN = "PriceN";

	/** Set Price New	  */
	public void setPriceN (BigDecimal PriceN);

	/** Get Price New	  */
	public BigDecimal getPriceN();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name WeightPay */
    public static final String COLUMNNAME_WeightPay = "WeightPay";

	/** Set Weight Payment	  */
	public void setWeightPay (BigDecimal WeightPay);

	/** Get Weight Payment	  */
	public BigDecimal getWeightPay();

    /** Column name XX_Boleta_ID */
    public static final String COLUMNNAME_XX_Boleta_ID = "XX_Boleta_ID";

	/** Set Boleta de Romana	  */
	public void setXX_Boleta_ID (int XX_Boleta_ID);

	/** Get Boleta de Romana	  */
	public int getXX_Boleta_ID();

	public org.eevolution.model.I_PP_Order getXX_Boleta() throws RuntimeException;

    /** Column name XX_Liquidation_ID */
    public static final String COLUMNNAME_XX_Liquidation_ID = "XX_Liquidation_ID";

	/** Set Liquidation	  */
	public void setXX_Liquidation_ID (int XX_Liquidation_ID);

	/** Get Liquidation	  */
	public int getXX_Liquidation_ID();

	public I_XX_Liquidation getXX_Liquidation() throws RuntimeException;

    /** Column name XX_LiquidationLine_ID */
    public static final String COLUMNNAME_XX_LiquidationLine_ID = "XX_LiquidationLine_ID";

	/** Set Liquidation Line	  */
	public void setXX_LiquidationLine_ID (int XX_LiquidationLine_ID);

	/** Get Liquidation Line	  */
	public int getXX_LiquidationLine_ID();

    /** Column name XX_NetWeight */
    public static final String COLUMNNAME_XX_NetWeight = "XX_NetWeight";

	/** Set Net Weight	  */
	public void setXX_NetWeight (BigDecimal XX_NetWeight);

	/** Get Net Weight	  */
	public BigDecimal getXX_NetWeight();
}
