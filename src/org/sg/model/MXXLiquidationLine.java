/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Yamel Senih 24/06/2011, 16:11
 *
 */
public class MXXLiquidationLine extends X_XX_LiquidationLine{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXLiquidationLine(Properties ctx, int XX_LiquidationLine_ID, String trxName) {
		super(ctx, XX_LiquidationLine_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXLiquidationLine(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}	
}
