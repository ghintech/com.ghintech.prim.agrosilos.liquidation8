/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.sg.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for XX_Liquidation
 *  @author Adempiere (generated) 
 *  @version Release 3.6.0LTS - $Id$ */
public class X_XX_Liquidation extends PO implements I_XX_Liquidation, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20120124L;

    /** Standard Constructor */
    public X_XX_Liquidation (Properties ctx, int XX_Liquidation_ID, String trxName)
    {
      super (ctx, XX_Liquidation_ID, trxName);
      /** if (XX_Liquidation_ID == 0)
        {
			setC_BPartner_ID (0);
			setC_Invoice_ID (0);
			setDateDoc (new Timestamp( System.currentTimeMillis() ));
// @#Date@
			setDocumentNo (null);
			setGrandTotal (Env.ZERO);
			setM_Product_ID (0);
			setTotalWeight (Env.ZERO);
			setXX_Liquidation_ID (0);
        } */
    }

    /** Load Constructor */
    public X_XX_Liquidation (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_XX_Liquidation[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (I_C_BPartner)MTable.get(getCtx(), I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getC_BPartner_ID()));
    }

	public I_C_Charge getC_Charge() throws RuntimeException
    {
		return (I_C_Charge)MTable.get(getCtx(), I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_Invoice getC_Invoice() throws RuntimeException
    {
		return (I_C_Invoice)MTable.get(getCtx(), I_C_Invoice.Table_Name)
			.getPO(getC_Invoice_ID(), get_TrxName());	}

	/** Set Invoice.
		@param C_Invoice_ID 
		Invoice Identifier
	  */
	public void setC_Invoice_ID (int C_Invoice_ID)
	{
		if (C_Invoice_ID < 1) 
			set_Value (COLUMNNAME_C_Invoice_ID, null);
		else 
			set_Value (COLUMNNAME_C_Invoice_ID, Integer.valueOf(C_Invoice_ID));
	}

	/** Get Invoice.
		@return Invoice Identifier
	  */
	public int getC_Invoice_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Invoice_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_C_PaySelection getC_PaySelection() throws RuntimeException
    {
		return (I_C_PaySelection)MTable.get(getCtx(), I_C_PaySelection.Table_Name)
			.getPO(getC_PaySelection_ID(), get_TrxName());	}

	/** Set Payment Selection.
		@param C_PaySelection_ID 
		Payment Selection
	  */
	public void setC_PaySelection_ID (int C_PaySelection_ID)
	{
		if (C_PaySelection_ID < 1) 
			set_Value (COLUMNNAME_C_PaySelection_ID, null);
		else 
			set_Value (COLUMNNAME_C_PaySelection_ID, Integer.valueOf(C_PaySelection_ID));
	}

	/** Get Payment Selection.
		@return Payment Selection
	  */
	public int getC_PaySelection_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_PaySelection_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Document Date.
		@param DateDoc 
		Date of the Document
	  */
	public void setDateDoc (Timestamp DateDoc)
	{
		set_ValueNoCheck (COLUMNNAME_DateDoc, DateDoc);
	}

	/** Get Document Date.
		@return Date of the Document
	  */
	public Timestamp getDateDoc () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDoc);
	}

	/** Set Document No.
		@param DocumentNo 
		Document sequence number of the document
	  */
	public void setDocumentNo (String DocumentNo)
	{
		set_ValueNoCheck (COLUMNNAME_DocumentNo, DocumentNo);
	}

	/** Get Document No.
		@return Document sequence number of the document
	  */
	public String getDocumentNo () 
	{
		return (String)get_Value(COLUMNNAME_DocumentNo);
	}

	/** Set Grand Total.
		@param GrandTotal 
		Total amount of document
	  */
	public void setGrandTotal (BigDecimal GrandTotal)
	{
		set_Value (COLUMNNAME_GrandTotal, GrandTotal);
	}

	/** Get Grand Total.
		@return Total amount of document
	  */
	public BigDecimal getGrandTotal () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrandTotal);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_ValueNoCheck (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Total Lines.
		@param TotalLines 
		Total of all document lines
	  */
	public void setTotalLines (BigDecimal TotalLines)
	{
		set_Value (COLUMNNAME_TotalLines, TotalLines);
	}

	/** Get Total Lines.
		@return Total of all document lines
	  */
	public BigDecimal getTotalLines () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalLines);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set TotalWeight.
		@param TotalWeight TotalWeight	  */
	public void setTotalWeight (BigDecimal TotalWeight)
	{
		set_Value (COLUMNNAME_TotalWeight, TotalWeight);
	}

	/** Get TotalWeight.
		@return TotalWeight	  */
	public BigDecimal getTotalWeight () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalWeight);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** XX_Cancel_Liquidation AD_Reference_ID=1000040 */
	public static final int XX_CANCEL_LIQUIDATION_AD_Reference_ID=1000040;
	/** Canceled Liquidation = Y */
	public static final String XX_CANCEL_LIQUIDATION_CanceledLiquidation = "Y";
	/** Cancel Liquidation = N */
	public static final String XX_CANCEL_LIQUIDATION_CancelLiquidation = "N";
	/** Set Cancel Liquidation.
		@param XX_Cancel_Liquidation Cancel Liquidation	  */
	public void setXX_Cancel_Liquidation (String XX_Cancel_Liquidation)
	{

		set_Value (COLUMNNAME_XX_Cancel_Liquidation, XX_Cancel_Liquidation);
	}

	/** Get Cancel Liquidation.
		@return Cancel Liquidation	  */
	public String getXX_Cancel_Liquidation () 
	{
		return (String)get_Value(COLUMNNAME_XX_Cancel_Liquidation);
	}

	public I_C_Invoice getXX_CreditMemo() throws RuntimeException
    {
		return (I_C_Invoice)MTable.get(getCtx(), I_C_Invoice.Table_Name)
			.getPO(getXX_CreditMemo_ID(), get_TrxName());	}

	/** Set Credit Memo.
		@param XX_CreditMemo_ID Credit Memo	  */
	public void setXX_CreditMemo_ID (int XX_CreditMemo_ID)
	{
		if (XX_CreditMemo_ID < 1) 
			set_Value (COLUMNNAME_XX_CreditMemo_ID, null);
		else 
			set_Value (COLUMNNAME_XX_CreditMemo_ID, Integer.valueOf(XX_CreditMemo_ID));
	}

	/** Get Credit Memo.
		@return Credit Memo	  */
	public int getXX_CreditMemo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_CreditMemo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Cuota Arrocera.
		@param XX_Cuota_Arrocera Cuota Arrocera	  */
	public void setXX_Cuota_Arrocera (BigDecimal XX_Cuota_Arrocera)
	{
		set_Value (COLUMNNAME_XX_Cuota_Arrocera, XX_Cuota_Arrocera);
	}

	/** Get Cuota Arrocera.
		@return Cuota Arrocera	  */
	public BigDecimal getXX_Cuota_Arrocera () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_Cuota_Arrocera);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public I_C_Invoice getXX_DebitMemo() throws RuntimeException
    {
		return (I_C_Invoice)MTable.get(getCtx(), I_C_Invoice.Table_Name)
			.getPO(getXX_DebitMemo_ID(), get_TrxName());	}

	/** Set Debit Memo.
		@param XX_DebitMemo_ID Debit Memo	  */
	public void setXX_DebitMemo_ID (int XX_DebitMemo_ID)
	{
		if (XX_DebitMemo_ID < 1) 
			set_Value (COLUMNNAME_XX_DebitMemo_ID, null);
		else 
			set_Value (COLUMNNAME_XX_DebitMemo_ID, Integer.valueOf(XX_DebitMemo_ID));
	}

	/** Get Debit Memo.
		@return Debit Memo	  */
	public int getXX_DebitMemo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_DebitMemo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Liquidation.
		@param XX_Liquidation_ID Liquidation	  */
	public void setXX_Liquidation_ID (int XX_Liquidation_ID)
	{
		if (XX_Liquidation_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_XX_Liquidation_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_XX_Liquidation_ID, Integer.valueOf(XX_Liquidation_ID));
	}

	/** Get Liquidation.
		@return Liquidation	  */
	public int getXX_Liquidation_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_XX_Liquidation_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Quote Amount.
		@param XX_QuoteAmt Quote Amount	  */
	public void setXX_QuoteAmt (BigDecimal XX_QuoteAmt)
	{
		throw new IllegalArgumentException ("XX_QuoteAmt is virtual column");	}

	/** Get Quote Amount.
		@return Quote Amount	  */
	public BigDecimal getXX_QuoteAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_XX_QuoteAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}