/**
 * 
 */
package org.sg.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Yamel Senih 24/06/2011, 16:11
 *
 */
public class MXXLiquidation extends X_XX_Liquidation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7194108480610070579L;
	
	public MXXLiquidation(Properties ctx, int XX_Liquidation_ID, String trxName) {
		super(ctx, XX_Liquidation_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MXXLiquidation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
}
