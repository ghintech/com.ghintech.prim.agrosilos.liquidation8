/**
 * 
 */
package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;

import org.compiere.model.MInOutLine;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MTax;
import org.compiere.model.Tax;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.sg.model.MXXLiquidation;

import com.ghintech.prim.agrosilos.liquidation.base.CustomProcess;

/**
 * @author Dayana Mendoza 26/08/2011, 15:33 PA
 *
 */
public class CreateInvoiceFromLiq extends CustomProcess {

	private int m_C_Invoice_ID = 0;
	
	private MInvoice m_Invoice = null;
	
	private int m_XX_Liquidation_ID = 0;
	
	private int m_C_BPartner_ID = 0;
	
	//private int m_C_charge_ID = 0;
	
	/* (non-Javadoc)
	 * @see org.compiere.process.SvrProcess#prepare()
	 */
	@Override
	protected void prepare() {
		for (ProcessInfoParameter para : getParameter()) {
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			else if (name.equals("C_BPartner_ID"))
				m_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("XX_Liquidation_ID"))
				m_XX_Liquidation_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
			m_C_Invoice_ID = getRecord_ID();
	}


	@Override
	protected String doIt() throws Exception {
		BigDecimal v_XX_NetWeight = Env.ZERO;
		//BigDecimal weightAcond = Env.ZERO;
		//BigDecimal priceN = Env.ZERO;
		BigDecimal v_Price = Env.ZERO;
		BigDecimal v_LiqAmt = Env.ZERO;
		BigDecimal v_PriceInvoice = Env.ZERO;
		int v_XX_LiquidationLine_ID = 0;
		
		m_Invoice = new MInvoice(Env.getCtx(), m_C_Invoice_ID, get_TrxName());
		
		/**
		 * Formula para el calculo de la diferencia de Peso
		 * Price Invoice = Liquidation Amount / Net Weight
		 * 
		 * */
		
		if(m_Invoice.getC_BPartner_ID() == m_C_BPartner_ID){
			String sql = new String("SELECT llq.WeightPay, llq.Price, llq.PriceN, llq.Amt, llq.LiqAmt, " +
					"iol.M_InOutLine_ID, llq.XX_NetWeight, (llq.LiqAmt/llq.XX_NetWeight) PriceInvoice, llq.XX_LiquidationLine_ID " +
					"FROM XX_Liquidation lq " +
					"INNER JOIN XX_LiquidationLine llq ON(llq.XX_Liquidation_ID = lq.XX_Liquidation_ID) " +
					"INNER JOIN PP_Order pp ON(pp.PP_Order_ID = llq.XX_Boleta_ID) " +
					"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND pp.M_Product_ID = lo.M_Product_ID) " +
					"INNER JOIN M_InOutLine iol ON(iol.C_OrderLine_ID = lo.C_OrderLine_ID) " +
					"INNER JOIN M_InOut io ON(io.M_InOut_ID = iol.M_InOut_ID) " +
					"WHERE io.DocStatus IN ('CO', 'CL') " +
					"AND lq.XX_Cancel_Liquidation = 'N' " +
					"AND lq.Processed = 'N' " +
					"AND lq.IsActive = 'Y' " +
					"AND lq.AD_Client_ID = ? " +
					"AND lq.C_BPartner_ID = ? " +
					"AND lq.XX_Liquidation_ID = ?");
		
			log.fine("consultaRealizados SQL = " + sql);
			//System.out.println(sql);
			PreparedStatement pstmt = null;
			try {
				pstmt = DB.prepareStatement (sql, get_TrxName());
				pstmt.setInt(1, getAD_Client_ID());
				pstmt.setInt(2, m_Invoice.getC_BPartner_ID());
				pstmt.setInt(3, m_XX_Liquidation_ID);
				ResultSet rs = pstmt.executeQuery();

				while(rs.next()){
					
					v_Price = rs.getBigDecimal("Price");
					
					v_LiqAmt = rs.getBigDecimal("LiqAmt");
					
					int m_M_InOutLine_ID = rs.getInt("M_InOutLine_ID");

					v_XX_NetWeight = rs.getBigDecimal("XX_NetWeight");
					
					v_PriceInvoice = rs.getBigDecimal("PriceInvoice");
			
					v_XX_LiquidationLine_ID = rs.getInt("XX_LiquidationLine_ID");
					
					MInOutLine inOutLine = new MInOutLine(Env.getCtx(), m_M_InOutLine_ID, null);
				
					MInvoiceLine iLine = new MInvoiceLine(getCtx(), 0, get_TrxName());
				
					iLine.setC_Invoice_ID(m_Invoice.getC_Invoice_ID());
					iLine.setM_InOutLine_ID(inOutLine.getM_InOutLine_ID());
					//order line
					iLine.setC_OrderLine_ID(inOutLine.getC_OrderLine_ID());
					iLine.setM_Product_ID(inOutLine.getM_Product_ID());
					iLine.setC_UOM_ID(inOutLine.getC_UOM_ID());

					//	Quantity and Amount
					iLine.setQty(v_XX_NetWeight);	
					iLine.setPrice(v_PriceInvoice);
					iLine.setPriceActual(v_PriceInvoice);
					iLine.setPriceEntered(v_PriceInvoice);
					iLine.setPriceList(v_Price);
					iLine.setLineNetAmt(v_LiqAmt);
					iLine.setC_Tax_ID(Tax.getExemptTax(getCtx(), m_Invoice.getAD_Org_ID(), get_TrxName())); 
					
					//	Liquidation Line
					iLine.set_ValueOfColumn("XX_LiquidationLine_ID", v_XX_LiquidationLine_ID);
					iLine.setAD_Org_ID(m_Invoice.getAD_Org_ID());
					iLine.saveEx();
					//if(!iLine.save()) {
						//throw new AdempiereException("@C_InvoiceLine_ID@");
					//}
				}
				
				MXXLiquidation liq = new MXXLiquidation(getCtx(), m_XX_Liquidation_ID, get_TrxName());
				liq.setC_Invoice_ID(m_Invoice.getC_Invoice_ID());
				if(!liq.save()){
					throw new AdempiereException("@XX_Liquidation_ID@");
				}
				
			} catch (SQLException e) {
				throw new AdempiereException("@C_InvoiceLine_ID@" + ":" + e.getMessage());
			}
		} else {
			throw new AdempiereException( Msg.getMsg(Env.getCtx(), "SGNotConBPartner"));
		}
		return "OK";
	}

}
