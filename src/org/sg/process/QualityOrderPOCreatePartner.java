package org.sg.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutLine;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MProduct;
import org.compiere.model.MUOMConversion;
import org.compiere.model.Tax;
import org.compiere.model.X_C_Order;
import org.compiere.model.X_M_InOut;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.eevolution.model.X_PP_Order;

import com.ghintech.prim.agrosilos.liquidation.base.CustomProcess;

/**
 * @author Yamel Senih
 *
 */
public class QualityOrderPOCreatePartner extends CustomProcess {

	/** Warehouse			*/
	private int			p_M_Warehouse_ID = 0;
	/**	Doc Date From		*/
	private Timestamp	p_DateDoc;
	/**	Doc Date From		*/
	private Timestamp	dateDoc;
	/** BPartner			*/
	private int 		p_C_BPartner_ID = 0;
	
	private int 		p_C_DocType_ID = 0;
	
	/**	UOM					*/
	private int 		p_C_UOM_ID =0;
	/**	UOM To				*/
	private int 		p_C_UOM_To_ID = 0;
	
	private int			m_created_Order = 0;
	
	private boolean		p_ConsolidateDocument = false;
	
	/**	C_DocTypeTarget_ID	*/
	private int			p_C_DocTypeTarget_ID = 0;
	
	/** Order				*/
	private MOrder		m_order = null;
	
	private int			m_Ordenes = 0;
	/** Quality Order		*/
	private X_PP_Order	qOrder = null;
	
	/**	Created				*/
	private int 		m_created_InOut = 0;
	/**	Actividad				*/
	private int 		p_C_Activity_ID = 0;
	
	/**	Errores					*/
	private int 		m_Errors = 0;
	
	/**	Summary				*/
	private StringBuffer summary = new StringBuffer();
	
	private String		message = null;
	
	@Override
	protected void prepare() {

		for (ProcessInfoParameter para : getParameter())
		{
			String name = para.getParameterName();
			if (para.getParameter() == null)
				;
			//fjviejo e-evolution petrocasa end
			else if (name.equals("M_Warehouse_ID"))
				p_M_Warehouse_ID = para.getParameterAsInt();
			else if (name.equals("M_Locator_ID"))
				para.getParameterAsInt();
			else if (name.equals("DateDoc"))
				p_DateDoc = (Timestamp)para.getParameter();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para.getParameterAsInt();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para.getParameterAsInt();
			else if (name.equals("ConsolidateDocument"))
				p_ConsolidateDocument = "Y".equals(para.getParameter());
			else if (name.equals("C_DocTypeTarget_ID"))
				p_C_DocTypeTarget_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_ID"))
				p_C_UOM_ID = para.getParameterAsInt();
			else if (name.equals("C_UOM_To_ID"))
				p_C_UOM_To_ID = para.getParameterAsInt();
			else if (name.equals("C_Activity_ID"))
				p_C_Activity_ID = para.getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}	//	prepare

	@Override
	protected String doIt() /*throws Exception*/ {
		String sql = null;
		sql = "SELECT * FROM PP_Order o "
				+ "WHERE DocStatus IN('CO') "
				+ "AND XX_TipoPesada = 'PM' "
				+ "AND XX_Estatus_Calidad NOT IN('R') "
				+ "AND AD_Client_ID=? "
				+ "AND EXISTS(SELECT pc.* "
				+ "FROM C_Period p "
				+ "INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) "
				+ "WHERE pc.DocBaseType IN('MQO', 'MMS', 'POO') "
				+ "AND pc.PeriodStatus = 'O' "
				+ "AND p.StartDate <= DateOrdered AND p.EndDate >= DateOrdered) ";
			if (p_C_BPartner_ID != 0)
				sql += " AND C_BPartner_ID=?";
			if (p_C_DocType_ID != 0)
				sql += " AND C_DocType_ID=?";
		
		sql += " ORDER BY PP_Order_ID ";
		log.info(sql.toString());
		
		//System.out.println(sql);
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql, get_TrxName());
			int index = 1;
			pstmt.setInt(index++, getAD_Client_ID());
			if (p_C_BPartner_ID != 0)
				pstmt.setInt(index++, p_C_BPartner_ID);
			if (p_C_DocType_ID != 0)
				pstmt.setInt(index++, p_C_DocType_ID);
		}catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		
		return generarOrdenes(pstmt);
	}	//	doIt
	
	/*
	 *	generarOrdenes
	 */
	private String generarOrdenes(PreparedStatement pstmt){
		try{
			ResultSet rs = pstmt.executeQuery();
			if(p_ConsolidateDocument){
				consolidado(rs);
			}else{
				noConsolidado(rs);
			}		
			if (pstmt != null)
				pstmt.close ();
			pstmt = null;
		} catch (Exception e){
			m_Errors ++;
			log.log(Level.SEVERE, "", e);
		}		
		
		summary.append("@C_Order_ID@ = ");
		summary.append(m_created_Order);
		summary.append(" @M_InOut_ID@ = ");
		summary.append(m_created_InOut);
		summary.append(" Errors = ");
		summary.append(m_Errors);

		addLog(summary.toString());
		
		if(message != null)
			return message;
		return summary.toString();
		
	}	//	generarOrdenes
	
	/*
	 * consolidado
	 */
	private String consolidado(ResultSet rs) throws SQLException{
		if(rs.next ()){
			qOrder = new X_PP_Order (getCtx(), rs, get_TrxName());
			crearOrden();
			do{
				qOrder = new X_PP_Order (getCtx(), rs, get_TrxName());
				crearLineas();
				closeQOrder();
			}while (rs.next ());
			completeOrder();
		}
		return "@Created@ = " + m_created_Order;
	}	//	consolidado
	
	private void consultaRealizados(){
		String sql = new String("SELECT count(*) ordenes " +
				"FROM PP_Order pp " +
				"INNER JOIN C_OrderLine lo ON(lo.PP_Order_ID = pp.PP_Order_ID AND lo.M_Product_ID = pp.M_Product_ID) " +
				"WHERE pp.PP_Order_ID = ? ");
		log.fine("consultaRealizados SQL = " + sql);
		
		//System.err.println("SQL " + sql);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int cont = 1;
			pstmt = DB.prepareStatement (sql, get_TrxName());
			pstmt.setInt(cont++, qOrder.getPP_Order_ID());
			rs = pstmt.executeQuery();
			if(rs.next()){
				m_Ordenes = rs.getInt("ordenes");
			}
		} catch (SQLException e) {
			throw new AdempiereException(Msg.translate(getCtx(), "SGBDError") + ":" + e.getMessage());
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	}
	
	
	/*
	 * consolidado
	 */
	private void noConsolidado(ResultSet rs) throws SQLException{
		int m_C_BPartner_ID = 0;
		MBPartner m_bpartner = null;
		while (rs.next ()){
			System.out.println("Pesada " + rs.getString("DocumentNo"));
			qOrder = new X_PP_Order (getCtx(), rs, get_TrxName());
			consultaRealizados();
			if(p_C_UOM_To_ID == 0){
				m_Errors ++;
				throw new AdempiereException(Msg.translate(getCtx(), "SGCUOMNotF"));
			}
			if(p_DateDoc == null){
				dateDoc = qOrder.getDateOrdered();
			} else {
				dateDoc = p_DateDoc;
			}
			
			if(m_Ordenes == 0){
				m_C_BPartner_ID = qOrder.get_ValueAsInt("C_BPartner_ID");
				m_bpartner = MBPartner.get(getCtx(), m_C_BPartner_ID);
				if(MBPartnerLocation.getForBPartner(getCtx(), m_bpartner.getC_BPartner_ID(), get_TrxName()) == null 
						|| MBPartnerLocation.getForBPartner(getCtx(), m_bpartner.getC_BPartner_ID(), get_TrxName()).length == 0){
					message = Msg.translate(getCtx(), "C_BPartner_ID") + ": " + 
							m_bpartner.getValue() + " - " + m_bpartner.getName() +
							" No tiene Dirección";
					break;
				} else {
					crearOrden();
					crearLineas();
					procOrdenGenInPro();
					closeQOrder();
				}
			}
		}
	}	//	consolidado
	
	/*
	 *	crearOrden
	 */
	private void crearOrden(){
		MBPartner m_bpartner = MBPartner.get(getCtx(), qOrder.get_ValueAsInt("C_BPartner_ID"));
		m_order = new MOrder(getCtx(), 0, get_TrxName());
		m_order.setAD_Org_ID(qOrder.getAD_Org_ID());
		m_order.setDatePromised(dateDoc);
		m_order.setDateAcct(dateDoc);
		m_order.setDateOrdered(dateDoc);
		m_order.setC_DocTypeTarget_ID(p_C_DocTypeTarget_ID);
		m_order.setBPartner(m_bpartner);
		m_order.setM_PriceList_ID(m_bpartner.getPO_PriceList_ID());
		m_order.setC_PaymentTerm_ID(m_bpartner.getPO_PaymentTerm_ID());
		m_order.setDateAcct(dateDoc);
		m_order.setC_Activity_ID(p_C_Activity_ID);
		m_order.setIsSOTrx(false);
		if(p_M_Warehouse_ID != 0){
			m_order.setM_Warehouse_ID(p_M_Warehouse_ID);
		} else {
			m_order.setM_Warehouse_ID(qOrder.getM_Warehouse_ID());
		}
		m_order.setPriorityRule(qOrder.getPriorityRule());
			
	}	//	crearOrden
	
	/**
	 * Calculo de Peso Acondicionado
	 * @return BigDecimal
	 */
	/*private BigDecimal calcAcond(){
		BigDecimal humedad = Env.ZERO;
		BigDecimal impureza = Env.ZERO;
		BigDecimal pAcond = Env.ZERO;

		String sql = new String("SELECT round(XX_Fn_getHumedad(br.Weight3, br.Humedad), 2) p_Humedad, " +
				"round(XX_Fn_getImpureza(br.Weight3, br.Humedad, br.Impurezas), 2) p_Impureza, " +
				"round(XX_Fn_getPAcond(br.Weight3, br.Humedad, br.Impurezas), 2) p_Pagar " +
				"FROM XX_RV_Boleta_Romana br " +
				"WHERE br.PP_Order_ID = ?");

		log.fine("calcAcond SQL = " + sql);
		
		BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			pstmt.setInt(1, qOrder.getPP_Order_ID());
			rs = pstmt.executeQuery();
			if(rs.next()){
				humedad = rs.getBigDecimal("p_Humedad");
				impureza = rs.getBigDecimal("p_Impureza");
				pAcond = rs.getBigDecimal("p_Pagar");
			} else {
				m_Errors ++;
				new AdempiereException("@M_AttributeSetInstance_ID@ Invalid");
			}
		}catch (Exception e) {
			m_Errors ++;
			log.log(Level.SEVERE, sql.toString(), e);
		} finally {
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		log.info("Impureza = " + impureza);
		log.info("Humedad = " + humedad);
		log.info("Peso Neto = " + pNeto);
		log.info("Peso Acondicionado = " + pAcond);
		
		return pAcond;
	}	//	calcAcond*/
	
	/*
	 * 	crearLineas
	 */
	private void crearLineas(){
		
		MProduct product = (MProduct)qOrder.getM_Product();
		
		BigDecimal pNeto = (BigDecimal)qOrder.get_Value("Weight3");
		
		/*	Conversion	*/
		BigDecimal rConversion = MUOMConversion.getRate(getCtx(), p_C_UOM_ID, p_C_UOM_To_ID);
		
		if(m_order.save()){			
			MOrderLine m_orderLine = new MOrderLine(m_order);
			int C_Tax_ID = Tax.get (getCtx(), product.getM_Product_ID(), 0, dateDoc, dateDoc,
					m_order.getAD_Org_ID(), m_order.getM_Warehouse_ID(), m_order.getBill_Location_ID(), m_order.getC_BPartner_Location_ID(),
					"Y".equals(m_order.isSOTrx()));
			m_orderLine.setOrder(m_order);

			m_orderLine.setProduct(product);
			m_orderLine.setC_UOM_ID(product.getC_UOM_ID());
			
			m_orderLine.setAD_Org_ID(m_order.getAD_Org_ID());
			
			m_orderLine.setPrice();
			
			m_orderLine.setQty(pNeto.multiply(rConversion));
			m_orderLine.setQtyOrdered(pNeto.multiply(rConversion));
			
			m_orderLine.setDateOrdered(qOrder.getDateOrdered());
			m_orderLine.setDatePromised(qOrder.getDatePromised());
			m_orderLine.setDiscount(Env.ZERO);
			
			m_orderLine.setC_Currency_ID(m_order.getC_Currency_ID());
			m_orderLine.setM_Warehouse_ID(m_order.getM_Warehouse_ID());
			m_orderLine.setC_Tax_ID(C_Tax_ID);
			m_orderLine.set_ValueOfColumn("PP_Order_ID", qOrder.getPP_Order_ID());
			m_orderLine.setM_AttributeSetInstance_ID(qOrder.getM_AttributeSetInstance_ID());
			m_orderLine.setC_Activity_ID(p_C_Activity_ID);
			m_orderLine.setDiscount(Env.ZERO);
			
			
			m_order.setDescription(qOrder.getDocumentNo());
			
			if(m_orderLine.save()){
				completeOrder();
			} else{
				m_Errors ++;
				throw new AdempiereException("@SaveError@ @C_OrderLine_ID@");
			}
		} else{
			m_Errors ++;
			throw new AdempiereException("@SaveError@ @C_Order_ID@");
		}
	}	//	crearLineas

	/*
	 * 	Complete Order
	 */
	private void completeOrder()
	{
		if (m_order != null)
		{
			//	Fails if there is a confirmation
			if (!m_order.processIt(DocAction.ACTION_Complete))
				log.warning("Failed: " + m_order);
			m_order.setDocStatus(X_C_Order.DOCSTATUS_Completed);
			m_order.setDocAction(X_C_Order.DOCACTION_Close);
			m_order.setProcessed(true);
			m_order.save();
			//
			addLog(m_order.getC_Order_ID(), m_order.getDateOrdered(), null, m_order.getDocumentNo());
			m_created_Order++;
		}
	}	//	completeOrder
	
	private void procOrdenGenInPro(){
		/*
		 * Se crean las lineas a partir de las lineas de la orden
		 */
		
		MDocType docType = new MDocType(getCtx(), p_C_DocTypeTarget_ID, get_TrxName());
		
		//	Se obtiene el documento destino de Recepciones
		int m_C_DocTypeShip_ID =docType.getC_DocTypeShipment_ID();
		
		MOrderLine lineasOrden[] = m_order.getLines();
		
		//	Se Obtienen las lineas de las Ordenes
		MOrderLine lineaOrden = lineasOrden[0];System.out.println("Get " + lineaOrden);
		/*
		 * Crea el encabezado de la Recepcion a partir de la orden
		 */
		MInOut recep = new MInOut(m_order, m_C_DocTypeShip_ID, m_order.getDateOrdered());
		
		MBPartner m_bpartner = MBPartner.get(getCtx(), m_order.getC_BPartner_ID());
			
		X_PP_Order ppOrder = new X_PP_Order(getCtx(), lineaOrden.get_ValueAsInt("PP_Order_ID"), get_TrxName());
			
		recep.setAD_Org_ID(lineaOrden.getAD_Org_ID());
		recep.setDateAcct(dateDoc);
		recep.setDateOrdered(dateDoc);
		recep.setDateReceived(dateDoc);
		recep.setM_Warehouse_ID(lineaOrden.getM_Warehouse_ID());
		recep.setC_Activity_ID(p_C_Activity_ID);
		recep.setBPartner(m_bpartner);
		recep.setSalesRep_ID(m_bpartner.getSalesRep_ID());		
		recep.setIsSOTrx(false);
		recep.setC_Order_ID(m_order.getC_Order_ID());
		/*
		 * Se crea la linea de la recepción a partir de la linea de la Orden
		 */
		
		System.out.println("Recepcion " + recep);
		
		if(recep.save()){
			MInOutLine lineaRecepcion = new MInOutLine(getCtx(), 0, get_TrxName());
			lineaRecepcion.setM_InOut_ID(recep.getM_InOut_ID());
			lineaRecepcion.setAD_Org_ID(m_order.getAD_Org_ID());
			lineaRecepcion.setOrderLine(lineaOrden, 0, Env.ZERO);
			
			lineaRecepcion.setC_Activity_ID(p_C_Activity_ID);
			
			lineaRecepcion.setQty(lineaOrden.getQtyOrdered());
			lineaRecepcion.setQtyEntered(lineaOrden.getQtyEntered());
			
			lineaRecepcion.setC_UOM_ID(lineaOrden.getC_UOM_ID());
			
			lineaRecepcion.setAD_Org_ID(lineaOrden.getAD_Org_ID());
			
			int m_M_Locator_ID = ppOrder.get_ValueAsInt("M_Locator_ID"); 
			if(m_M_Locator_ID != 0){
				lineaRecepcion.setM_Locator_ID(m_M_Locator_ID);
			}
			if(lineaRecepcion.save()){
				completeReceived(recep);
			} else {
				m_Errors ++;
				throw new AdempiereException("@SaveError@ @M_InOutLine_ID@");
			}
		} else {
			m_Errors ++;
			throw new AdempiereException("@SaveError@ @M_InOut_ID@");
		}
	}
	
	/*
	 * Close Quality Order
	 */
	private void closeQOrder(){
		if(qOrder != null){
			qOrder.setDocStatus(X_C_Order.DOCSTATUS_Closed);
			qOrder.setDocAction(X_C_Order.DOCACTION_None);
			//qOrder.setProcessed(true);
			qOrder.save();
		}
	}	//	closeQOrder
	
	private void completeReceived(MInOut recep){
		/*
		 * Completa la Recepción de Material
		 */
		if (recep != null){
			recep.completeIt();
			recep.setDocStatus(X_M_InOut.DOCSTATUS_Completed);
			recep.setDocAction(X_M_InOut.DOCACTION_Close);
			recep.setProcessed(true);
			recep.save();
			//
			addLog(recep.getM_InOut_ID(), recep.getDateAcct(), null, recep.getDocumentNo());
			m_created_InOut++;
		}
	}
	
}