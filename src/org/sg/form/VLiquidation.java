package org.sg.form;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.window.FDialog;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
//import org.compiere.apps.ADialog;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.util.ZKUpdateUtil;
//import org.compiere.apps.StatusBar;
//import org.compiere.apps.form.FormFrame;
//import org.compiere.apps.form.FormPanel;
//import org.compiere.grid.ed.VDate;
import org.compiere.model.MLookup;
//import org.compiere.grid.ed.VLookup;
//import org.compiere.minigrid.MiniTable;
import org.compiere.model.MCharge;
import org.compiere.model.MLookupFactory;
//import org.compiere.plaf.CompiereColor;
//import org.compiere.swing.CLabel;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
//import org.compiere.swing.CPanel;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.TrxRunnable;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;

public class VLiquidation extends Liquidation
	implements IFormController, EventListener<Event>, WTableModelListener,ValueChangeListener
{
	private Panel panel = new Panel();

	public VLiquidation(){
		init();
	}
	/**
	 *	Initialize Panel
	 *  @param WindowNo window
	 *  @param frame frame
	 */
	public void init ()
	{
		//m_WindowNo = WindowNo;
		//m_frame = frame;
		
		Env.setContext(Env.getCtx(), m_WindowNo, "IsSOTrx", "N");   //  defaults to no
		try	{
			dynInit();
			zkInit();
		//	frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
		//	frame.getContentPane().add(statusBar, BorderLayout.SOUTH);
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
	}	//	init

	MathContext mc = new MathContext(2);
	
	MLookup lookupSPP;
	
	/**	Window No					*/
	private int         m_WindowNo = 0;
	/**	Organization				*/
	private int 		m_AD_Org_ID = 0;
	/**	Warehouse					*/
	private int 		m_M_Warehouse_ID = 0;
	/**	Sales Rep					*/
	private int 		m_C_BPartner_ID = 0;
	/**	Product						*/
	private int 		m_M_Product_ID = 0;
	/**	Total Weight Liquidation	*/
	private BigDecimal 	totalLiq = Env.ZERO;
	 
	/**	Cuota de Arroz				*/
	private BigDecimal 	cuotaArroz = Env.ZERO;
	
	/**	Charge						*/
	private int 		m_C_Charge_ID = 0;
	private MCharge		charge = null;
	/**	FormFrame					*/
	//private FormFrame 	m_frame;

	//private Panel mainPanel = new Panel();
	private Borderlayout mainLayout = new Borderlayout();
	private Panel parameterPanel = new Panel();
	private Panel loadLiqPanel = new Panel();
	private Grid parameterLayout = GridFactory.newGridLayout();
	//	Warehouse
	private Label warehouseLabel = new Label();
	private WSearchEditor warehouseSearch = null;
	//	Charge Credit Memo and Quote Rice
	private Label chargeLabel = new Label();
	private WSearchEditor chargeSearch = null;
	//	Product Liquidation
	private Label mProductLabel = new Label();
	private WSearchEditor mProductSearch = null;
	//	Business Partner
	private WSearchEditor bPartnerSearch = null;
	private Label bPartnerLabel = new Label();
	//	Liquidation Table
	private WListbox liqTable = new WListbox();

	private Panel liqPanel = new Panel();

	private Label liqLabel = new Label();

	private Grid liquidationLayout =GridFactory.newGridLayout();

	private Label liqInfo = new Label();
	
	//private Panel liqScrollPane = new Panel();

	private Grid loadLiquidationLayout = GridFactory.newGridLayout();
	private Label differenceLabel = new Label();

	private Button gLiquidationButton = new Button();

	private StatusBarPanel statusBar = new StatusBarPanel();

	private Label organizationLabel = new Label();
	private WSearchEditor organizationPick = null;
	
	//	Liquidation Date
	private Label labelDateDoc = new Label();
	private WDateEditor fieldDateDoc = new WDateEditor();
	private CustomForm form = new CustomForm();
	/**
	 *  Static Init
	 *  @throws Exception
	 */
	private void zkInit() throws Exception
	{
		//CompiereColor.setBackground(panel);
		//
		form.appendChild(mainLayout);
		//mainPanel.appendChild(mainLayout);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("100%");
		//
		parameterPanel.appendChild(parameterLayout);
		loadLiqPanel.appendChild(loadLiquidationLayout);
		
		warehouseLabel.setText(Msg.translate(Env.getCtx(), "M_Warehouse_ID"));
		chargeLabel.setText(Msg.translate(Env.getCtx(), "C_Charge_ID"));
		bPartnerLabel.setText(Msg.translate(Env.getCtx(), "SGProducer"));
		mProductLabel.setText(Msg.translate(Env.getCtx(), "M_Product_ID"));
		//	Liquidation Date
		labelDateDoc.setText(Msg.translate(Env.getCtx(), "DateDoc"));
		
		//liqLabel.setRequestFocusEnabled(false);
		liqLabel.setText(" " + Msg.translate(Env.getCtx(), "XX_Liquidation_ID"));
		liqPanel.appendChild(liquidationLayout);

		//liqInfo.setHorizontalAlignment(SwingConstants.RIGHT);
		//liqInfo.setHorizontalTextPosition(SwingConstants.RIGHT);
		liqInfo.setText(Msg.translate(Env.getCtx(), "SGLiquidationLine") 
				+ " = " +  Env.ZERO  + " -- "
				+ Msg.translate(Env.getCtx(), "SGCuotaSum") 
				+ " = " + Env.ZERO  + " -- " 
				+ Msg.translate(Env.getCtx(), "SGLiquidationSum") 
				+ " = [" + Env.ZERO + "] ");
		gLiquidationButton.setLabel(Msg.getMsg(Env.getCtx(), "SGGenerateLiquidation"));
		gLiquidationButton.addActionListener(this);
		
		//liqScrollPane.setPreferredSize(new Dimension(200, 200));
		//mainPanel.add(parameterPanel, BorderLayout.NORTH);
		North north = new North();
		north.setStyle("border: none");
		north.appendChild(parameterPanel);
		Rows rows = null;
		Row row = null;
		rows = parameterLayout.newRows();
			
		row = rows.newRow();
		organizationLabel.setText(Msg.translate(Env.getCtx(), "AD_Org_ID"));
		row.appendChild(organizationLabel);
		row.appendChild(organizationPick.getComponent());
		row.appendChild(warehouseLabel);
		row.appendChild(warehouseSearch.getComponent());
		
		row = rows.newRow();
		row.appendChild(bPartnerLabel);
		row.appendChild(bPartnerSearch.getComponent());
		row.appendChild(mProductLabel);
		row.appendChild(mProductSearch.getComponent());
		
		row = rows.newRow();
		
		row.appendChild(labelDateDoc);
		row.appendChild(fieldDateDoc.getComponent());
		row.appendChild(chargeLabel);
		row.appendChild(chargeSearch.getComponent());
		
		
		Center center = new Center();
		center.setStyle("border: none");
		center.appendChild(liqPanel);
		//center.setWidth("100%");
		//liqPanel.setWidth("100%");
		 ZKUpdateUtil.setWidth((HtmlBasedComponent)liquidationLayout, (String)"100%");
	        ZKUpdateUtil.setHeight((HtmlBasedComponent)liquidationLayout, (String)"100%");
	        liquidationLayout.setStyle("margin:0; padding:0; position: absolute; align: center; valign: center; overflow-y: auto;");
	        liquidationLayout.makeNoStrip();
	        liquidationLayout.setOddRowSclass("even");
	    center.setAutoscroll(true);
	    
		rows = liquidationLayout.newRows();
		
		row = rows.newRow();
		
		row.appendChild(liqLabel);
		//
		
		row = rows.newRow();
		row.appendChild(liqTable);
		//liquidationLayout.setWidth("100%");
		//liqScrollPane.getViewport().appendChild(liqTable, null);
		
		
		
		
		//mainPanel.add(loadLiqPanel, BorderLayout.SOUTH);
		South south = new South();
		south.setStyle("border: none");
		south.appendChild(loadLiqPanel);
		rows = loadLiquidationLayout.newRows();
		
		row = rows.newRow();
		row.appendChild(liqInfo);
		row.appendChild(differenceLabel);
		row.appendChild(gLiquidationButton);
		
		
		
		//
		mainLayout.appendChild(north);
		mainLayout.appendChild(center);
		mainLayout.appendChild(south);
	}   //  jbInit

	/**
	 * 	Dispose
	 */
	public void dispose()
	{
		//if(isFromStatement()) {
			form.dispose();
		//} else {
			//SessionManager.getAppDesktop().closeActiveWindow();
		//}
	}	//	dispose

	/**
	 *  Dynamic Init (prepare dynamic fields)
	 *  @throws Exception if Lookups cannot be initialized
	 */
	public void dynInit() throws Exception
	{
			
		// Organization filter selection
		int AD_Column_ID = 839;		//	C_Period.AD_Org_ID (needed to allow org 0)
		MLookup lookupOrg = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		organizationPick = new WSearchEditor("AD_Org_ID", false, false, true, lookupOrg);
		organizationPick.setValue(Env.getAD_Org_ID(Env.getCtx()));
		organizationPick.addValueChangeListener(this);
		
		AD_Column_ID = 1151;		//	M_Warehouse.M_Warehouse_ID
		MLookup lookupWar = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		warehouseSearch = new WSearchEditor("M_Warehouse_ID", false, false, true, lookupWar);
		warehouseSearch.setValue(Env.getContextAsInt(Env.getCtx(), "#M_Warehouse_ID"));
		warehouseSearch.addValueChangeListener(this);
		
		AD_Column_ID = 1001372;		//	XX_Liquidation.C_Charge_ID
		MLookup lookupCharge = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, AD_Column_ID, DisplayType.TableDir);
		chargeSearch = new WSearchEditor("C_Charge_ID", true, false, true, lookupCharge);
		chargeSearch.addValueChangeListener(this);
		
		AD_Column_ID = 1000857;		//	XX_Liquidation.C_BPartner_ID
		MLookup lookupBP = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		bPartnerSearch = new WSearchEditor("C_BPartner_ID", true, false, true, lookupBP);
		bPartnerSearch.addValueChangeListener(this);
		
		AD_Column_ID = 53623;		//	PP_Order.M_Product_ID
		MLookup lookupPro = MLookupFactory.get(Env.getCtx(), m_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		mProductSearch = new WSearchEditor("M_Product_ID", true, false, true, lookupPro);
		mProductSearch.addValueChangeListener(this);
		
		statusBar.setStatusLine(Msg.getMsg(Env.getCtx(), "SGLiquidation"));
		statusBar.setStatusDB("");
		
		//	Liquidation Date
		fieldDateDoc.setMandatory(true);
		fieldDateDoc.setValue(new Timestamp(System.currentTimeMillis()));
		
	}   //  dynInit
	
	/**************************************************************************
	 *  Action Listener.
	 *  - MultiCurrency
	 *  - Allocate
	 *  @param e event
	 */
	public void onEvent(Event e) throws Exception {
	
	
		if(e.getName().equals("onClick")){
			Object value = bPartnerSearch.getValue();
			m_C_BPartner_ID = ((Integer)(value != null? value: 0)).intValue();
			if(m_C_BPartner_ID != 0){
				value = mProductSearch.getValue();
				m_M_Product_ID = ((Integer)(value != null? value: 0)).intValue();
				if(m_M_Product_ID != 0){
					if(m_C_Charge_ID != 0){
						if(totalLiq.compareTo(Env.ZERO) > 0){
							saveData();
						} else {
							FDialog.info(m_WindowNo, panel, Msg.translate(Env.getCtx(), "SGSumZero"));
						}
					} else {
						FDialog.info(m_WindowNo, panel, Msg.translate(Env.getCtx(), "SGCharge"));
					}
				} else {
					FDialog.info(m_WindowNo, panel, Msg.translate(Env.getCtx(), "SGNotMProduct"));
				}
			} else {
				FDialog.info(m_WindowNo, panel, Msg.translate(Env.getCtx(), "SGNotBPartner"));
			}
		}
		
	}   //  actionPerformed

	/**
	 *  Table Model Listener.
	 *  - Recalculate Totals
	 *  @param e event
	 */
	public void tableChanged(WTableModelEvent e)
	{	
		//boolean isUpdate = (e.getType() == TableModelEvent.UPDATE);
		boolean isUpdate = (e.getType() == WTableModelEvent.CONTENTS_CHANGED);
		//  Not a table update
		if (!isUpdate)
		{
			calculate();
			return;
		}
		
		int row = e.getFirstRow();
		int col = e.getColumn();
		if(col == HUMEDAD_EDIT 
				|| col == IMPUREZA_EDIT 
				|| col == PRICE_LIST_EDIT) {
			calLiquidation(liqTable, row);
		}
		
		if(chargeSearch.getValue() != null){
			calculate();
		} else {
			liqInfo.setText("*** " + Msg.translate(Env.getCtx(), "SGQuote") + " *** ");
		}
	}   //  tableChanged

	/**
	 *  Vetoable Change Listener.
	 *  
	 *  @param e event
	 */
	public void valueChange (ValueChangeEvent e)
	{
		String name = e.getPropertyName();
		Object value = e.getNewValue();
		log.config(name + " = " + value);
		
		if(name.equals("AD_Org_ID") || 
				name.equals("M_Warehouse_ID")){
			loadBallots();
		} else if(name.equals("C_BPartner_ID")){
			bPartnerSearch.setValue(value);
			loadBallots();
		} else if(name.equals("M_Product_ID")){
			mProductSearch.setValue(value);
			loadBallots();
		} else if(name.equals("C_Charge_ID")){
			//value = chargeSearch.getValue();
			m_C_Charge_ID = ((Integer)(value != null? value: 0)).intValue();
			if(m_C_Charge_ID != 0)
				charge = new MCharge(Env.getCtx(), m_C_Charge_ID, null);
			
		}
			
		if(m_C_Charge_ID != 0){
			calculate();
		} else {
			liqInfo.setText("*** " + Msg.translate(Env.getCtx(), "SGQuote") + " *** ");
		}
	}   //  vetoableChange
	
	public void loadBallots()
	{
		//checkBPartner();
		String name = organizationPick.getLabel().toString();
		Object value = organizationPick.getValue();
		m_AD_Org_ID = ((Integer)(value != null? value: 0)).intValue();
		log.config(name + "=" + value);
		
		name = warehouseSearch.getLabel().toString();
		value = warehouseSearch.getValue();
		m_M_Warehouse_ID = ((Integer)(value != null? value: 0)).intValue();
		log.config(name + "=" + value);
		
		name = chargeSearch.getLabel().toString();
		value = chargeSearch.getValue();
		m_C_Charge_ID = ((Integer)(value != null? value: 0)).intValue();
		if(m_C_Charge_ID != 0)
			charge = new MCharge(Env.getCtx(), m_C_Charge_ID, null);
		log.config(name + "=" + value);
		
		name = bPartnerSearch.getLabel().toString();
		value = bPartnerSearch.getValue();
		m_C_BPartner_ID = ((Integer)(value != null? value: 0)).intValue();
		log.config(name + "=" + value);
		
		name = mProductSearch.getLabel().toString();
		value = mProductSearch.getValue();
		m_M_Product_ID = ((Integer)(value != null? value: 0)).intValue();
		log.config(name + "=" + value);
		
		Vector<Vector<Object>> data = getPMData(m_AD_Org_ID, m_M_Warehouse_ID, m_C_BPartner_ID, m_M_Product_ID, liqTable);
		Vector<String> columnNames = getPMColumnNames();
		
		//  Remove previous listeners
		liqTable.getModel().removeTableModelListener(this);
		
		
		//  Set Model
		ListModelTable modelP = new ListModelTable(data);
		modelP.addTableModelListener(this);
		liqTable.setData(modelP,columnNames);
		setPMColumnClass(liqTable);
		//
	}
	
	/**
	 * 
	 * Calcula la diferencia de pesos y el peso total
	 *
	 */
	public void calculate(){
		int rows = liqTable.getRowCount();
		totalLiq = Env.ZERO;
		if(rows > 0){
			BigDecimal liquidation = Env.ZERO;
			BigDecimal netWeight = Env.ZERO;
			BigDecimal totalWeight = Env.ZERO;
			for (int i = 0; i < rows; i++) {
				if (((Boolean)liqTable.getValueAt(i, 0)).booleanValue()) {
					liquidation = (BigDecimal) liqTable.getValueAt(i, TOTAL_LIQUIDATION);
					netWeight = (BigDecimal) liqTable.getValueAt(i, WEIGHT_NET);
					
					totalLiq = totalLiq.add(liquidation);
					totalWeight = totalWeight.add(netWeight);
				}
			}
			if(m_C_Charge_ID != 0){
				cuotaArroz = charge.getChargeAmt();
			}
			
			liqInfo.setText(Msg.translate(Env.getCtx(), "SGLiquidationLine") 
					+ " = " +  totalLiq.setScale(m_Precision, RoundingMode.HALF_UP).doubleValue() + " -- "
					+ Msg.translate(Env.getCtx(), "SGCuotaSum") 
					+ " = " + totalWeight.multiply(cuotaArroz).setScale(m_Precision, RoundingMode.HALF_UP).doubleValue() + " -- " 
					+ Msg.translate(Env.getCtx(), "SGLiquidationSum") 
					+ " = [" + (!totalLiq.equals(Env.ZERO)?totalLiq.setScale(m_Precision, RoundingMode.HALF_UP).subtract(totalWeight.multiply(cuotaArroz)
							.setScale(m_Precision, RoundingMode.HALF_UP)).doubleValue():Env.ZERO) + "] ");
		} else {
			liqInfo.setText(Msg.translate(Env.getCtx(), "SGLiquidationLine") 
					+ " = " +  Env.ZERO  + " -- "
					+ Msg.translate(Env.getCtx(), "SGCuotaSum") 
					+ " = " + Env.ZERO  + " -- " 
					+ Msg.translate(Env.getCtx(), "SGLiquidationSum") 
					+ " = [" + Env.ZERO + "] ");
		}
	}
	
	/**************************************************************************
	 *  Save Data
	 */
	public void saveData()
	{
		try	{
			Trx.run(new TrxRunnable() 
			{
				public void run(String trxName)
				{
					//panel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					String msg = generateLiquidation(m_C_BPartner_ID, m_M_Product_ID, liqTable, (Timestamp)fieldDateDoc.getValue(), charge, trxName);
					statusBar.setStatusLine(msg);
					//panel.setCursor(Cursor.getDefaultCursor());
					FDialog.info(m_WindowNo, panel, msg);
					//dispose();
				}
			});
		}
		catch (Exception e)	{
			e.printStackTrace();
			FDialog.error(m_WindowNo, panel, "Error", e.getLocalizedMessage());
			statusBar.setStatusLine("Error" + e.getLocalizedMessage());
			dispose();
		}
		loadBallots();
		calculate();
	}   //  saveData

	

	

	@Override
	public ADForm getForm() {
		// TODO Auto-generated method stub
		return form;
	}
	
}