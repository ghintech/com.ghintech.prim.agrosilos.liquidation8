package org.sg.form;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.minigrid.IMiniTable;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCharge;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.Tax;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;
import org.eevolution.model.X_PP_Order;
import org.sg.model.MXXLiquidation;
import org.sg.model.MXXLiquidationLine;
import org.sg.model.MXXTicketVigilancia;

/**
 * @author Yamel Senih 24/06/2011, 12:57
 *
 */
public class Liquidation {

	/**	Logger			*/
	public static CLogger log = CLogger.getCLogger(Liquidation.class);
	

	public final int PP_ORDER = 1;
	public final int IN_OUT_LINE = 2;
	public final int HUMEDAD = 3;
	public final int IMPUREZA = 4;
	public final int WEIGHT_NET = 5;
	public final int PRICE_LIST = 6;
	public final int LINE_NET_AMT = 7;
	public final int HUMEDAD_EDIT = 8;
	public final int IMPUREZA_EDIT = 9;
	public final int PRICE_LIST_EDIT = 10;
	public final int HUMEDAD_WEIGHT = 11;
	public final int IMPUREZA_WEIGHT = 12;
	public final int WEIGHT_ACOND = 13;
	public final int TOTAL_LIQUIDATION = 14;
	
	protected int 	 m_Precision = 0;
	
	private MXXLiquidation liq = null;
	
	public Vector<Vector<Object>> getPMData(int p_AD_Org_ID, int p_M_Warehouse_ID, int p_C_BPartner_ID, int p_M_Product_ID, IMiniTable liqTable){
		/**
		 * Carga los datos de las Boletas de Romana
		 * 
		 * 
		 */
		
		m_Precision = MSysConfig.getIntValue("LIQUIDATION_PRECISION", 0, Env.getAD_Client_ID(Env.getCtx()), p_AD_Org_ID);
		
		int m_C_UOM_To_ID = MSysConfig.getIntValue("UOM_TO_LIQUIDATION_ID", 0, Env.getAD_Client_ID(Env.getCtx()), p_AD_Org_ID);
		
		log.info("UOM_TO_LIQUIDATION_ID=" + m_C_UOM_To_ID);
		
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		StringBuffer sql = new StringBuffer("SELECT br.PP_Order_ID, br.DocumentNo, lrec.M_InOutLine_ID, to_char(br.DateOrdered, 'DD/MM/YYYY'), " +
				"round(br.Humedad, 2) Humedad, round(br.Impurezas, 2) Impurezas, " +
				//	Add Precision
				"round(br.Weight3 * xx_fn_rateconversion_uom_from_to(NULL, br.C_UOM_ID, " + m_C_UOM_To_ID + "), 2) Weight3, round(loc.PriceList, " + m_Precision + ") PriceList, " +
				"round(loc.LineNetAmt, " + m_Precision + ") LineNetAmt, round(br.Humedad, 2) Humedad_Edit, " +
				//	Add Precision
				"round(br.Impurezas, 2) Impurezas_Edit, round(loc.PriceList, " + m_Precision + ") PriceList_Edit, " +
				"round(XX_Fn_getHumedadLiq(br.Weight3 * xx_fn_rateconversion_uom_from_to(NULL, br.C_UOM_ID, " + m_C_UOM_To_ID + "), br.Humedad, br.Humedad), 2) p_Humedad, " +
				"round(XX_Fn_getImpurezaLiq(br.Weight3 * xx_fn_rateconversion_uom_from_to(NULL, br.C_UOM_ID, " + m_C_UOM_To_ID + "), br.Impurezas, br.Impurezas), 2) p_Impureza, " +
				"round(XX_Fn_getPAcondLiq(br.Weight3 * xx_fn_rateconversion_uom_from_to(NULL, br.C_UOM_ID, " + m_C_UOM_To_ID + "), br.Humedad, br.Humedad, br.Impurezas, br.Impurezas), " + m_Precision + ") p_Pagar, " +
				//	Add Precision
				"round((XX_Fn_getPAcondLiq(br.Weight3 * xx_fn_rateconversion_uom_from_to(NULL, br.C_UOM_ID, " + m_C_UOM_To_ID + "), br.Humedad, br.Humedad, br.Impurezas, br.Impurezas) * loc.PriceList), " + m_Precision + ") m_Pagar " +
				"FROM XX_RV_Boleta_Romana br " +
				"INNER JOIN C_OrderLine loc ON(loc.PP_Order_ID = br.PP_Order_ID AND br.M_Product_ID = loc.M_Product_ID) " +
				"INNER JOIN C_Order oc ON(oc.C_Order_ID = loc.C_Order_ID) " +
				"INNER JOIN M_InOut rec ON(rec.C_Order_ID = oc.C_Order_ID) " +
				"INNER JOIN M_InOutLine lrec ON(lrec.M_InOut_ID = rec.M_InOut_ID) " +
				"INNER JOIN C_DocType td ON(td.C_DocType_ID = br.C_DocType_ID) " +
				"WHERE td.XX_Tipo_Q_Pesada = 'MP' " +
				"AND br.XX_SQ = 'A' " +
				"AND br.DocStatus IN('CO','CL') " +
				"AND rec.DocStatus IN('CO','CL') " +
				"AND br.PP_Order_ID NOT IN(SELECT llq.XX_Boleta_ID " +
				"FROM XX_Liquidation lq " +
				"INNER JOIN XX_LiquidationLine llq ON(llq.XX_Liquidation_ID = lq.XX_Liquidation_ID) " +
				"WHERE lq.XX_Cancel_Liquidation = 'N') " +
				"AND EXISTS(SELECT p.* FROM C_Period p INNER JOIN C_PeriodControl pc ON(pc.C_Period_ID = p.C_Period_ID) " +
				"WHERE pc.DocBaseType = 'MQO' " +
				"AND pc.PeriodStatus = 'O' " +
				"AND pc.AD_Client_ID = br.AD_Client_ID " +
				"AND p.StartDate <= br.DateOrdered AND p.EndDate >= br.DateOrdered) " +
				"AND br.AD_Client_ID = ? " +
				"AND br.C_BPartner_ID = ? " +
				"AND br.M_Product_ID = ? ");
		if (p_AD_Org_ID != 0)
			sql.append(" AND br.AD_Org_ID = ? ");
		if (p_M_Warehouse_ID != 0 )
			sql.append(" AND br.M_Warehouse_ID = ?");
		
		// role security
		sql = new StringBuffer( MRole.getDefault(Env.getCtx(), false).addAccessSQL( sql.toString(), "br", MRole.SQL_FULLYQUALIFIED, MRole.SQL_RO ) );
		
		sql.append(" GROUP BY br.PP_Order_ID, lrec.M_InOutLine_ID, br.DocumentNo, " +
				"br.DateOrdered, br.Humedad, br.Impurezas, br.Weight3, br.C_UOM_ID, loc.PriceList, " +
				"loc.LineNetAmt " +
				"ORDER BY br.PP_Order_ID ASC");
		
		log.fine("LiqSQL = " + sql.toString());
		
		//System.out.println(sql);
		
		try
		{
			int param = 1;
			
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			
			pstmt.setInt(param++, Env.getAD_Client_ID(Env.getCtx()));
			
			pstmt.setInt(param++, p_C_BPartner_ID);
			
			pstmt.setInt(param++, p_M_Product_ID);
			
			if (p_AD_Org_ID != 0)
				pstmt.setInt(param++, p_AD_Org_ID);
			if (p_M_Warehouse_ID != 0 )
				pstmt.setInt(param++, p_M_Warehouse_ID);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<Object> line = new Vector<Object>();
				line.add(false);       //  0-Selection
				KeyNamePair re = new KeyNamePair(rs.getInt(1), rs.getString(2));
				line.add(re);				       	//  1-DocumentNo
				KeyNamePair pp = new KeyNamePair(rs.getInt(3), rs.getString(4));
				line.add(pp);      					//  2-DateOrdered
				line.add(rs.getBigDecimal(5));      //  3-Humedad
				line.add(rs.getBigDecimal(6));      //	4-Impureza
				line.add(rs.getBigDecimal(7));		//	5-Weight3 "Peso Neto"
				line.add(rs.getBigDecimal(8));      //	6-PriceList
				line.add(rs.getBigDecimal(9));      //	7-LineNetAmt
				// Calculo
				line.add(rs.getBigDecimal(10));     //  8-Humedad Edit
				line.add(rs.getBigDecimal(11));     //	9-Impureza Edit
				line.add(rs.getBigDecimal(12));     //	10-PriceList
				line.add(rs.getBigDecimal(13));     //	11-Peso Humedad
				line.add(rs.getBigDecimal(14));     //	12-Peso Impureza
				line.add(rs.getBigDecimal(15));     //	13-Peso Acondicionado
				line.add(rs.getBigDecimal(16));     //	14-Monto a Pagar (Liquidacion)
	
				//
				data.add(line);
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
		
		return data;
	}
	
	/**
	 * Obtiene los nombres de las columnas de las Boletas
	 * @return
	 */
	public Vector<String> getPMColumnNames(){	
		//  Header Info
		Vector<String> columnNames = new Vector<String>();
		columnNames.add(Msg.getMsg(Env.getCtx(), "Select"));
		columnNames.add(Util.cleanAmp(Msg.translate(Env.getCtx(), "DocumentNo")));
		columnNames.add(Msg.translate(Env.getCtx(), "Date"));
		columnNames.add(Msg.translate(Env.getCtx(), "Humedad"));
		columnNames.add(Msg.translate(Env.getCtx(), "Impureza"));
		columnNames.add(Msg.translate(Env.getCtx(), "XX_NetWeight"));
		columnNames.add(Msg.translate(Env.getCtx(), "PriceList"));
		columnNames.add(Msg.translate(Env.getCtx(), "LineNetAmt"));
		columnNames.add(Msg.translate(Env.getCtx(), "HumedadN"));
		columnNames.add(Msg.translate(Env.getCtx(), "ImpurezaN"));
		columnNames.add(Msg.translate(Env.getCtx(), "PriceN"));
		columnNames.add(Msg.translate(Env.getCtx(), "HumedadW"));
		columnNames.add(Msg.translate(Env.getCtx(), "ImpurezaW"));
		columnNames.add(Msg.translate(Env.getCtx(), "WeightPay"));
		columnNames.add(Msg.translate(Env.getCtx(), "LiqAmt"));

		return columnNames;
	}
	
	/**
	 * Establece las clases de las columnas de las Boletas
	 * @param liqTable
	 */
	public void setPMColumnClass(IMiniTable liqTable){
		int i = 0;
		    
		liqTable.setColumnClass(i++, Boolean.class, false);			//  0-Selection
		liqTable.setColumnClass(i++, String.class, true);			//	1-DocumentNo
		liqTable.setColumnClass(i++, String.class, true);			//  2-DateOrdered
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	3-Humedad
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	4-Impureza
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	5-Weight3 "Peso Neto"
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	6-PriceList
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	7-LineNetAmt
		liqTable.setColumnClass(i++, BigDecimal.class, false);		//  8-Humedad Edit
		liqTable.setColumnClass(i++, BigDecimal.class, false);    	//	9-Impureza Edit
		liqTable.setColumnClass(i++, BigDecimal.class, false);     	//	10-PriceList
		liqTable.setColumnClass(i++, BigDecimal.class, true);     	//	11-Peso Humedad
		liqTable.setColumnClass(i++, BigDecimal.class, true);     	//	12-Peso Impureza
		liqTable.setColumnClass(i++, BigDecimal.class, true);     	//	13-Peso Acondicionado
		liqTable.setColumnClass(i++, BigDecimal.class, true);		//	14-Monto a Pagar
		//  Table UI
		liqTable.autoSize();
	}
	
	/**
	 * Obtiene el descuento del Productor
	 * @param p_C_BPartner_ID
	 * @return
	 */
	public BigDecimal getCuota(int p_C_BPartner_ID){
		BigDecimal cuota = Env.ZERO;
		MBPartner bPartner = new MBPartner(Env.getCtx(), p_C_BPartner_ID, null);
		String cuotaArroz = bPartner.get_ValueAsString("XX_Cuota_Arrocera");
		if(cuotaArroz != null)
			cuota = new BigDecimal(cuotaArroz);
		return cuota;
	}
	
	
	/**
	 * Hace el calculo de Liquidación de acuerdo con lo modificado en la tabla
	 */
	public void calLiquidation(IMiniTable liqTable, int row){
		
		
		BigDecimal pesoNeto = (BigDecimal)liqTable.getValueAt(row, WEIGHT_NET);
		BigDecimal humedad = (BigDecimal)liqTable.getValueAt(row, HUMEDAD);
		BigDecimal impureza = (BigDecimal)liqTable.getValueAt(row, IMPUREZA);
		BigDecimal humedadNeg = (BigDecimal)liqTable.getValueAt(row, HUMEDAD_EDIT);
		BigDecimal impurezaNeg = (BigDecimal)liqTable.getValueAt(row, IMPUREZA_EDIT);
		BigDecimal precioEdit = (BigDecimal)liqTable.getValueAt(row, PRICE_LIST_EDIT);
		
		String sql = new String("SELECT round(XX_fn_getHumedadLiq(?, ?, ?), 2) p_Humedad, " +
				"round(XX_fn_getImpurezaLiq(?, ?, ?), 2) p_Impureza, " +
				"round(XX_Fn_getPAcondLiq(?, ?, ?, ?, ?), 2) p_Pagar");
		try
		{
			int param = 1;
			
			PreparedStatement pstmt = DB.prepareStatement(sql.toString(), null);
			
			//	Humedad
			pstmt.setBigDecimal(param++, pesoNeto);
			pstmt.setBigDecimal(param++, humedad);
			pstmt.setBigDecimal(param++, humedadNeg);
			//	Impureza
			pstmt.setBigDecimal(param++, pesoNeto);
			pstmt.setBigDecimal(param++, impureza);
			pstmt.setBigDecimal(param++, impurezaNeg);
			//	Peso Acondicionado
			pstmt.setBigDecimal(param++, pesoNeto);
			pstmt.setBigDecimal(param++, humedad);
			pstmt.setBigDecimal(param++, humedadNeg);
			pstmt.setBigDecimal(param++, impureza);
			pstmt.setBigDecimal(param++, impurezaNeg);
			//	Monto a Liquidar
			/*pstmt.setBigDecimal(param++, pesoNeto);
			pstmt.setBigDecimal(param++, humedad);
			pstmt.setBigDecimal(param++, humedadNeg);
			pstmt.setBigDecimal(param++, impureza);
			pstmt.setBigDecimal(param++, impurezaNeg);
			pstmt.setBigDecimal(param++, precioLista);*/
			
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				System.out.println(rs.getBigDecimal(1)+" "+rs.getBigDecimal(2)+" "+rs.getBigDecimal(3));
				liqTable.setValueAt(rs.getBigDecimal(1), row, HUMEDAD_WEIGHT);   	//	11-Peso Humedad
				liqTable.setValueAt(rs.getBigDecimal(2), row, IMPUREZA_WEIGHT);		//	12-Peso Impureza
				liqTable.setValueAt(rs.getBigDecimal(3), row, WEIGHT_ACOND);		//	13-Peso Acondicionado
				BigDecimal montoPay = rs.getBigDecimal(3).multiply(precioEdit);
				liqTable.setValueAt(montoPay, row, TOTAL_LIQUIDATION);				//	14-Monto a Pagar (Liquidacion)
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}
	}
	
	/**
	 * Genera La orden de Carga con sus lineas.
	 * @param m_M_Shipper_ID
	 * @param m_XX_Conductor_ID
	 * @param m_XX_Vehiculo_ID
	 * @param orderLineTable
	 * @param m_C_Charge_ID
	 * @return
	 */
	public String generateLiquidation(int m_C_BPartner_ID, int m_M_Product_ID, IMiniTable liqTable, Timestamp dateDoc, MCharge charge, String trxName){
		String DocumentNo = null;
		int v_XX_Boleta_ID = 0;
		int m_gen = 0;
		int rows = liqTable.getRowCount();
		
		String m_DocumentNo = verifBoletas(liqTable, rows);
		
		if(m_DocumentNo == null){
			BigDecimal descuento = Env.ZERO;
			
			liq = new MXXLiquidation(Env.getCtx(), 0, trxName);
			liq.setDateDoc(dateDoc);
			liq.setC_BPartner_ID(m_C_BPartner_ID);
			liq.setM_Product_ID(m_M_Product_ID);
			liq.setGrandTotal(Env.ZERO);
			liq.setTotalLines(Env.ZERO);
			liq.setXX_Cuota_Arrocera(charge.getChargeAmt());
			liq.setC_Charge_ID(charge.getC_Charge_ID());
			
			if(liq.save()){
				DocumentNo = liq.getDocumentNo();
				MXXLiquidationLine lLiq = null;
				BigDecimal humedad = Env.ZERO; 
				BigDecimal humedadN = Env.ZERO;
				BigDecimal humedadW = Env.ZERO;
				BigDecimal impureza = Env.ZERO;
				BigDecimal impurezaN = Env.ZERO;
				BigDecimal impurezaW = Env.ZERO;
				BigDecimal price = Env.ZERO;
				BigDecimal priceN = Env.ZERO;
				BigDecimal weightAcond = Env.ZERO;
				BigDecimal totalLiquidation = Env.ZERO;
				BigDecimal amt = Env.ZERO;
				BigDecimal netWeight = Env.ZERO;
				BigDecimal weightTotal = Env.ZERO;
				BigDecimal grandTotal = Env.ZERO;
				BigDecimal totalNetWeight = Env.ZERO;
				
				for (int i = 0; i < rows; i++) {
					if (((Boolean)liqTable.getValueAt(i, 0)).booleanValue()) {
						lLiq = new MXXLiquidationLine(Env.getCtx(), 0, trxName);
						humedad = (BigDecimal) liqTable.getValueAt(i, HUMEDAD); 
						humedadN = (BigDecimal) liqTable.getValueAt(i, HUMEDAD_EDIT);
						humedadW = (BigDecimal) liqTable.getValueAt(i, HUMEDAD_WEIGHT);
						impureza = (BigDecimal) liqTable.getValueAt(i, IMPUREZA);
						impurezaN = (BigDecimal) liqTable.getValueAt(i, IMPUREZA_EDIT);
						impurezaW = (BigDecimal) liqTable.getValueAt(i, IMPUREZA_WEIGHT);
						price = (BigDecimal) liqTable.getValueAt(i, PRICE_LIST);
						priceN = (BigDecimal) liqTable.getValueAt(i, PRICE_LIST_EDIT);
						weightAcond = (BigDecimal) liqTable.getValueAt(i, WEIGHT_ACOND);
						totalLiquidation = (BigDecimal) liqTable.getValueAt(i, TOTAL_LIQUIDATION);
						amt = (BigDecimal) liqTable.getValueAt(i, LINE_NET_AMT);
						netWeight = (BigDecimal) liqTable.getValueAt(i, WEIGHT_NET);					
						KeyNamePair pp = (KeyNamePair) liqTable.getValueAt(i, PP_ORDER);
						int m_XX_Boleta_ID = (pp != null? pp.getKey(): 0);
						
						//	Totales	
						
						totalNetWeight = totalNetWeight.add(netWeight);
						weightTotal = weightTotal.add(weightAcond);
						grandTotal = grandTotal.add(totalLiquidation);
						
						lLiq.setXX_Liquidation_ID(liq.getXX_Liquidation_ID());
						lLiq.setHumedad(humedad);
						lLiq.setHumedadN(humedadN);
						lLiq.setHumedadW(humedadW);
						lLiq.setImpurezas(impureza);
						lLiq.setImpurezaN(impurezaN);
						lLiq.setImpurezaW(impurezaW);
						lLiq.setPrice(price);
						lLiq.setPriceN(priceN);
						lLiq.setAmt(amt);
						lLiq.setXX_NetWeight(netWeight);
						lLiq.setWeightPay(weightAcond);
						lLiq.setLiqAmt(totalLiquidation);
						lLiq.setXX_Boleta_ID(m_XX_Boleta_ID);//Arreglar el descuento en la liquidación
						//Ya está listo, Sólo era cuestión de tiempo
						
						v_XX_Boleta_ID = lLiq.getXX_Boleta_ID();
						
						if(lLiq.save()){
							m_gen ++;
						} else {
							throw new AdempiereException("@XX_LiquidationLine_ID@");
						}
					}
				}
					
				//	Totals
				descuento = totalNetWeight.multiply(charge.getChargeAmt()).setScale(m_Precision, RoundingMode.HALF_UP);
				liq.setTotalWeight(weightTotal);
				liq.setTotalLines(grandTotal.setScale(m_Precision, RoundingMode.HALF_UP));
				liq.setGrandTotal(grandTotal.setScale(m_Precision, RoundingMode.HALF_UP).subtract(descuento).setScale(m_Precision, RoundingMode.HALF_UP));
				
				//	create Credit Memo
				MInvoice ajust = createDocument(charge, totalNetWeight, 
						MDocType.DOCBASETYPE_APCreditMemo, liq.getC_BPartner_ID(), trxName);
				liq.setXX_CreditMemo_ID(ajust.getC_Invoice_ID());
				
				//	Document Association
				
				X_PP_Order m_XX_Boleta = new X_PP_Order(Env.getCtx(), v_XX_Boleta_ID, trxName);
				
				//	Document Quality
				
				int v_DocumentQuality_ID = m_XX_Boleta.get_ValueAsInt("DocumentQuality_ID");
				
				X_PP_Order m_XX_DocumentQ = new X_PP_Order(Env.getCtx(), v_DocumentQuality_ID, trxName);
				
				//	Ticket
				
				int v_TV_ID = m_XX_DocumentQ.get_ValueAsInt("XX_Ticket_Vigilancia_ID");
				
				MXXTicketVigilancia xx_Ticket = new MXXTicketVigilancia(Env.getCtx(), v_TV_ID, trxName);
				
				//	Business Partner Location
				
				MBPartnerLocation loc = (MBPartnerLocation) xx_Ticket.getC_BPartner_Location();
				
				int v_XX_Association_ID = loc.get_ValueAsInt("XX_Association_ID");
				
				if(v_XX_Association_ID == 0)
					throw new AdempiereException("SGNotAssociation");
				
				//	Debit Ajust
				
				ajust = createDocument(charge, totalNetWeight, 
						MDocType.DOCBASETYPE_APInvoice, v_XX_Association_ID, trxName);
				
				liq.setXX_DebitMemo_ID(ajust.getC_Invoice_ID());
				
				if(!liq.save()){
					throw new AdempiereException("@XX_Liquidation_ID@");
				}
			} else {
				throw new AdempiereException("@XX_Liquidation_ID@");
			}
			
			return Msg.translate(Env.getCtx(), "SGLiquidationGenerate") + " = [" + DocumentNo + "] || " +
						Msg.translate(Env.getCtx(), "SGLiquidationLineGenerate") + " = [" + m_gen + "]";
		} else {
			return "SGDuplicateWeight";
		}
		
		
	}	//	crearFactura
	
	/**
	 * Genera la Nota de Credito al Productor o Nota de Debito a la Asociacion 
	 * por concepto de Cuota Arrocera
	 * @author Yamel Senih 05/11/2011, 17:23:28
	 * @param charge
	 * @param totalNetWeight
	 * @param trxName
	 * @return
	 * @return MInvoice
	 */
	private MInvoice createDocument(MCharge charge, BigDecimal totalNetWeight, String v_C_DocTypeTarget_ID, int v_C_BPartner_ID, String trxName){
		MInvoice ajust = new MInvoice(Env.getCtx(), 0, trxName);
		
		//	Set Value Ajust from Invoice
		ajust.setAD_Org_ID(liq.getAD_Org_ID());
		ajust.setDateInvoiced(liq.getDateDoc());
		ajust.setDateAcct(liq.getDateDoc());
		
		ajust.setC_DocTypeTarget_ID(v_C_DocTypeTarget_ID);
		
		MBPartner bPartner = new MBPartner(Env.getCtx(), v_C_BPartner_ID, trxName);
		
		ajust.setBPartner(bPartner);
		//	Sales Representative
		if(bPartner.getSalesRep_ID() != 0)
			ajust.setSalesRep_ID(bPartner.getSalesRep_ID());
		else
			ajust.setSalesRep_ID(Env.getAD_User_ID(Env.getCtx()));
		//	Liquidation
		ajust.set_ValueOfColumn("XX_Liquidation_ID", liq.getXX_Liquidation_ID());
		//	Description
		ajust.setDescription(Util.cleanAmp(Msg.translate(Env.getCtx(), "XX_Liquidation_ID")) + 
				" Nº:" + liq.getDocumentNo() + " -- " + bPartner.getName());
		
		if(ajust.save()){
			MInvoiceLine ajustLine = new MInvoiceLine(ajust);
			ajustLine.setC_Charge_ID(charge.getC_Charge_ID());
			//	Price
			ajustLine.setPrice(charge.getChargeAmt());
			ajustLine.setPriceList(charge.getChargeAmt());
			ajustLine.setPriceEntered(charge.getChargeAmt());
			ajustLine.setC_Tax_ID(Tax.getExemptTax(Env.getCtx(), liq.getAD_Org_ID(), trxName));
			//	Qty
			ajustLine.setQty(totalNetWeight);
			ajustLine.setQtyInvoiced(totalNetWeight);
			
			if(ajustLine.save()){
				//	Complete Ajust
				ajust.processIt(DocAction.ACTION_Complete);
				if(ajust.save()){
					//	set Credit Memo Liquidation
					return ajust;
					//liq.setXX_CreditMemo_ID(ajust.getC_Invoice_ID());
				} else {
					throw new AdempiereException("@C_Invoice_ID@");
				}
			} else {
				throw new AdempiereException("@C_Invoice_ID@");
			}
		}
		return null;
	}
	

	/**
	 * Verifica si las boletas de romana que se van a liquidar existen en otra liquidación activa
	 * @param liqTable
	 * @param rows
	 * @return
	 * @throws SQLException
	 */
	private String verifBoletas(IMiniTable liqTable, int rows){
		String m_DocumentNo = null;
		StringBuffer sql = new StringBuffer("SELECT pp.DocumentNo " +
				"FROM XX_Liquidation lq " +
				"INNER JOIN XX_LiquidationLine llq ON(llq.XX_Liquidation_ID = lq.XX_Liquidation_ID) " +
				"INNER JOIN PP_Order pp ON(pp.PP_Order_ID = llq.XX_Boleta_ID) ");
		//	Where
		
		sql.append("WHERE lq.XX_Cancel_Liquidation = 'N' AND llq.XX_Boleta_ID IN(0");
		
		for(int i = 0; i < rows; i++){
			if (((Boolean)liqTable.getValueAt(i, 0)).booleanValue()) {
				int ID = ((KeyNamePair)liqTable.getValueAt(i, 1)).getKey();
				sql.append(",");
				sql.append(ID);
			}
		}
		
		sql.append(")");
		System.out.println(sql);
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			rs = pstmt.executeQuery();
			if (rs.next()){
				m_DocumentNo = rs.getString("DocumentNo");
			}
			DB.close(rs, pstmt);
		}catch (Exception e) {
			log.log(Level.SEVERE, sql.toString(), e);
		}finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null; 
		}
		
		return m_DocumentNo;
	}
}
